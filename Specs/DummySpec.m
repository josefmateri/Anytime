//
//  DummySpec.m
//  Anytime
//
//  Created by Josef Materi on 10/19/14
//  Copyright (c) 2014 Josef Materi. All rights reserved.
//

SpecBegin(Dummy)

describe(@"YourApp", ^{

    it(@"should be thoroughly tested, y'heard?", ^{
        expect(YES).to.beTruthy();
    });

});

SpecEnd
