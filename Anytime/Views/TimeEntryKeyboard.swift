//
//  TimeEntryKeyboard.swift
//  Anytime
//
//  Created by Josef Materi on 23.10.14.
//  Copyright (c) 2014 Crush & Lovely. All rights reserved.
//

import UIKit
import Cartography

enum KeyboardState {
    case Open
    case Closed
}

protocol TimeEntryKeyboardDelegate {
    func timeEntryKeyboard(keyboard: TimeEntryKeyboard, didSelectDigit: Int)
    func timeEntryKeyboard(keyboard: TimeEntryKeyboard, didSwitch mode: KeyboardMode)
}

class TimeEntryKeyboard: UIView, UICollectionViewDelegate, UICollectionViewDataSource, KeyboardModeViewDelegate {

    var collectionView : UICollectionView!
    var keyboardModeView : KeyboardModeView!
    var delegate : TimeEntryKeyboardDelegate?
    var modeLayoutDatasource : KeyboardModeLayoutDatasourceProtocol = KeyboardModeDigitLayout()
    
    var mode : KeyboardMode {
        get {
            return keyboardModeView.mode
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.clipsToBounds = true
        
        var flowLayout = UICollectionViewFlowLayout()
        flowLayout.itemSize = CGSizeMake(65, 65)
        flowLayout.minimumInteritemSpacing = 15
        flowLayout.minimumLineSpacing = 20
    
        
        self.backgroundColor = UIColor(white: 0, alpha: 0.25)
        
        collectionView = UICollectionView(frame: CGRectZero, collectionViewLayout: flowLayout)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.registerClass(TimeEntryKeyboardCollectionViewCell.classForCoder(), forCellWithReuseIdentifier: "cell")
        collectionView.backgroundColor = UIColor.clearColor()
        collectionView.delaysContentTouches = false
        
        self.addSubview(collectionView)

        
        layout(collectionView) { view in
            view.width == 240
            view.height == 320
            view.right == view.superview!.right - 20
            view.centerY == view.superview!.centerY
        }
        
        collectionView .reloadData()
        
        
        var vibrantLineTop = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.Light))
        vibrantLineTop.alpha = 0.25
        
        self.addSubview(vibrantLineTop)
        
        layout(vibrantLineTop, self) { vibrantLine, digitsView in
            vibrantLine.width == digitsView.width
            vibrantLine.top == digitsView.top
            vibrantLine.centerX == digitsView.centerX
            vibrantLine.height == 1
        }
        
        var vibrantLine = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.Light))
        vibrantLine.alpha = 0.25
        
        self.addSubview(vibrantLine)
        
        var layoutConstraint : NSLayoutConstraint!
        
        layout(vibrantLine, self) { vibrantLine, digitsView in
            vibrantLine.width == digitsView.width
            layoutConstraint = vibrantLine.top == digitsView.bottom - 1
            vibrantLine.centerX == digitsView.centerX
            vibrantLine.height == 1
        }

        keyboardModeView = KeyboardModeView(frame: CGRectZero)
        keyboardModeView.delegate = self
        self .addSubview(keyboardModeView)
        
        layout(keyboardModeView) { view in
            view.left == view.superview!.left + 20
            view.centerY == view.superview!.centerY
            view.width == 76
            view.height == view.superview!.height
        }
        
        hideKeyboard()
        
    }
    
    func showKeyboard() {
        collectionView.transform = CGAffineTransformIdentity
        keyboardModeView.collectionView.transform = CGAffineTransformIdentity
    }
    
    func hideKeyboard() {
        collectionView.transform = CGAffineTransformMakeScale(0.75, 0.75)
        keyboardModeView.collectionView.transform = CGAffineTransformMakeScale(0.75, 0.75)
    }
    
    func heightForState(state: KeyboardState) -> CGFloat {
        switch state {
        case .Open:
            return 360
        case .Closed:
            return 0
        }
    }
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        var collectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as TimeEntryKeyboardCollectionViewCell

        collectionViewCell.backgroundColor = UIColor(white: 0, alpha: 0.0)
        collectionViewCell.layer.cornerRadius = collectionViewCell.bounds.size.width / 2
        collectionViewCell.layer.borderColor = UIColor(white: 1, alpha: 0.15).CGColor
        collectionViewCell.layer.borderWidth = 1
        collectionViewCell.hidden = false
        collectionViewCell.imageView.image = nil
        
        var keyData = modeLayoutDatasource.keyDataForIndexPath(indexPath)
        collectionViewCell.titleLabel.text = keyData.title
        collectionViewCell.hidden = !keyData.enabled

    
        return collectionViewCell
    }
    
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 12
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        var cell = collectionView.cellForItemAtIndexPath(indexPath)
        cell?.selected = true

        var keyData = modeLayoutDatasource.keyDataForIndexPath(indexPath)
        
        delegate?.timeEntryKeyboard(self, didSelectDigit: keyData.value)
    }

    func digitFromIndexPath(indexPath: NSIndexPath) -> Int {
        if (indexPath.row < 9) {
            return indexPath.row + 1
        }
        if (indexPath.row == 10) {
            return 0
        }
        return 0
    }

}

extension TimeEntryKeyboard {
    func keyboardModeView(keyboardModeView: KeyboardModeView, didSwitchMode mode: KeyboardMode) {
        
        if mode == .Time {
            self.modeLayoutDatasource = KeyboardModeTimeLayout()
        }
        
        if mode == .Digits {
            self.modeLayoutDatasource = KeyboardModeDigitLayout()
        }
        
        if mode == .History {
            self.modeLayoutDatasource = KeyboardModeHistoryLayout()
        }
        
        self.collectionView .reloadData()
        
        delegate?.timeEntryKeyboard(self, didSwitch: mode)
    }
}