//
//  DigitsView.swift
//  Anytime
//
//  Created by Josef Materi on 20.10.14.
//  Copyright (c) 2014 Crush & Lovely. All rights reserved.
//

import UIKit
import Cartography

protocol DigitsViewDelegate {
    func digitsView(digitsView: DigitsView, didSelectDigitAtIndex index: Int)
}

class DigitsView: UIView {

    var delegate : DigitsViewDelegate?
    
    var firstHoursDigit : UILabel!
    var secondHoursDigit : UILabel!
    
    var firstMinutesDigit : UILabel!
    var secondMinutesDigit : UILabel!
    
    var colon : UILabel!
    
    var titleLabel : UILabel!
    
    var digits : [UILabel]!
    
    var font : UIFont! {
        didSet  {
            for digit in [firstHoursDigit, secondHoursDigit, colon, firstMinutesDigit, secondMinutesDigit] {
                digit.font = self.font
            }
        }
    }
    
    var textColor : UIColor! {
        didSet {
            for digit in [firstHoursDigit, secondHoursDigit, colon, firstMinutesDigit, secondMinutesDigit, titleLabel] {
                digit.textColor = self.textColor
            }
        }
    }
    
    var selectedIndex : Int? {
        
        
        didSet {
            
            if selectedIndex < countElements(digitNumbers) {
                for digit in digits {
                    digit.stopBlinking()
                }
                if let s = self.selectedIndex {
                    var digit = digits[min(s, countElements(digits) - 1)]
                    digit.blinkWithInterval(0.75)
                }
            } else {
                selectedIndex = countElements(digitNumbers) - 1
            }
            
        }
    }
    
    var minutes : Int? {
        didSet {
            self.digitNumbers = digitsFromMinutes(self.minutes)
            updateDigitLables(digitNumbers)
        }
    }
    
    
    var digitNumbers : [Int] = [0,0,0,0] {
        didSet {
            var newMinutes = minutesFromDigits(digitNumbers)
            if newMinutes != self.minutes {
                self.minutes = newMinutes
            }
            updateDigitLables(digitNumbers)
        }
    }
    
    
    override init(frame: CGRect) {
        
        super.init(frame: frame)
        
        
        self.selectedIndex = nil
        
        font = UIFont(name: "HelveticaNeue-Light", size: 112)
        textColor = UIColor.whiteColor()
        
        firstHoursDigit = UILabel(frame: CGRectZero)
        firstHoursDigit.text = "0"
        firstHoursDigit.font = font
        firstHoursDigit.textColor = textColor
        self.addSubview(firstHoursDigit)
        
        secondHoursDigit = UILabel(frame: CGRectZero)
        secondHoursDigit.text = "0"
        secondHoursDigit.font = font
        secondHoursDigit.textColor = textColor
        self.addSubview(secondHoursDigit)
        
        colon = UILabel(frame: CGRectZero)
        colon.text = ":"
        colon.font = font
        colon.textColor = textColor
        self.addSubview(colon)
        
        firstMinutesDigit = UILabel(frame: CGRectZero)
        firstMinutesDigit.text = "0"
        firstMinutesDigit.font = font
        firstMinutesDigit.textColor = textColor
        self.addSubview(firstMinutesDigit)
        
        secondMinutesDigit = UILabel(frame: CGRectZero)
        secondMinutesDigit.text = "0"
        secondMinutesDigit.font = font
        secondMinutesDigit.textColor = textColor
        self.addSubview(secondMinutesDigit)

        
        titleLabel = UILabel(frame: CGRectZero)
        self.addSubview(titleLabel)
        titleLabel.textColor = self.textColor

        colon.setTranslatesAutoresizingMaskIntoConstraints(false)
        titleLabel.setTranslatesAutoresizingMaskIntoConstraints(false)
        
        layoutWithoutTitle()
        
        digits = [firstHoursDigit, secondHoursDigit, firstMinutesDigit, secondMinutesDigit]
        updateDigitLables(self.digitNumbers)
        
        var tapGesture = UITapGestureRecognizer(target: self, action: "didTapDigits:")
        self .addGestureRecognizer(tapGesture)
    }
    
    // MARK: Gesture Recognizers
    
    func didTapDigits(gesture: UITapGestureRecognizer) {
        
        var touchPoint = gesture.locationInView(self)
        
        for (index, digit) in enumerate(digits) {
            var newPoint = digit.convertPoint(touchPoint, fromView: self)
            if (digit.pointInside(newPoint, withEvent: nil) == true) {
                self.selectedIndex = index
                
            }
        }
        if let select = self.selectedIndex {
            delegate?.digitsView(self, didSelectDigitAtIndex: select)
        }
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    // MARK: Title configuration
    
    func setTitle(title: String?) {
        if let s = title {
            self.titleLabel.text = s
            layoutWithTitle()
        } else {
            layoutWithoutTitle()
        }
    }
    
    // MARK: Time configuration
    
    func setTime(#string: String?) {
        if let s = string {
            self.minutes = minutesFromString(s)
        }
    }
    
    
    func setTime(#minutes: Int?) {
        if let m = minutes {
            self.minutes = m
        }
    }
    
    // MARK: Digit configuration
    

    func setDigitAtSelectedIndex(digit: Int) -> Bool {
        
        var number = abs(digit % 10)
        
        if let selectedIndex = self.selectedIndex {
            
            var oldDigit : Int = digitNumbers.atIndex(selectedIndex)
            
            var currentDigits = digitNumbers

            if selectedIndex < countElements(digitNumbers)  {
                
                var digitLabel = digits[selectedIndex]
                digitLabel.stopShake()
                currentDigits[selectedIndex] = number
                updateDigitLables(currentDigits)
                
            }
            
            if digitValid(digit, atIndex: selectedIndex) {
            
                self.minutes = minutesFromDigits(currentDigits)
                return true
            
            } else {
            
                self.digits[selectedIndex].shake { finished in
                    if finished {
                        self.minutes = self.minutesFromDigits(self.digitNumbers)
                    }
                }
    
                return false
            
            }

        }

        return false
    }

    private func updateDigitLables(digitLabels: [Int]?) {
        
        var newDigitNumbers : [Int]? = digitLabels
        
        if digitLabels == nil {
            newDigitNumbers = digitNumbers
        }
        
        if let numbers = newDigitNumbers {
            for (index, digit) in enumerate(digits) {
                digit.text = String(numbers[index])
            }
        }
        
    }
    
    func digitValid(digit: Int, atIndex index: Int) -> Bool {
        
        switch index {
        case 0:
            if digit > 2  { return false }
        case 1:
            if digit > 3 {
                if digitNumbers[0] < 2 {
                    return true
                }
                return false
            }
        case 2:
            if digit > 5 {
                return false
            }
        default:
            return true
        }
        
        return true
    }
    
    func jumpToNextDigit() -> Bool {
        
    
        if let s = self.selectedIndex {
            if s == countElements(digitNumbers) - 1 {
                return false
            } else {
                self.selectedIndex = s + 1
                return true
            }
        }
        return false
    
    }
    
    func selectAllDigits() {
        for digit in digits {
            digit.blinkWithInterval(0.75)
        }
    }
    
    func unselectAllDigits() {
        for digit in digits {
            digit.stopBlinking()
        }
    }
    
   
    // MARK: Layout
    
    private func layoutWithTitle() {
        
        self.removeConstraints(self.constraints())

        var colonCenter = NSLayoutConstraint(item: colon, attribute: NSLayoutAttribute.CenterX, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.CenterX, multiplier: 1, constant: 0)
        self.addConstraint(colonCenter)
        
//        self.addConstraint(NSLayoutConstraint(item: self, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: 160))
        
        var layoutConstraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-(20)-[titleLabel]-(0)-[colon]-(20)-|", options: NSLayoutFormatOptions.AlignAllCenterX, metrics: nil, views: ["titleLabel": titleLabel, "colon" : colon])
        
        self.addConstraints(layoutConstraints)
        
      layout(secondHoursDigit, colon) { secondHoursDigit, colon in
          secondHoursDigit.trailing == colon.leading
          secondHoursDigit.centerY == colon.centerY
      }
      
      layout(firstHoursDigit, secondHoursDigit) { firstHoursDigit, secondHoursDigit in
          firstHoursDigit.trailing == secondHoursDigit.leading
          firstHoursDigit.centerY == secondHoursDigit.centerY
      }
      
      layout(firstMinutesDigit, colon) { right, left in
          right.leading == left.trailing
          right.centerY == left.centerY
      }
      
      layout(secondMinutesDigit, firstMinutesDigit) { right, left in
          right.leading == left.trailing
          right.centerY == left.centerY
      }
    }
    
    private func layoutWithoutTitle() {
        
        self.removeConstraints(self.constraints())
        
        layout(colon) { view in
            view.centerX == view.superview!.centerX
            view.centerY == view.superview!.centerY
            
        }
        
        layout(secondHoursDigit, colon) { secondHoursDigit, colon in
            secondHoursDigit.trailing == colon.leading
            secondHoursDigit.centerY == colon.centerY
        }
        
        layout(firstHoursDigit, secondHoursDigit) { firstHoursDigit, secondHoursDigit in
            firstHoursDigit.trailing == secondHoursDigit.leading
            firstHoursDigit.centerY == secondHoursDigit.centerY
        }
        
        layout(firstMinutesDigit, colon) { right, left in
            right.leading == left.trailing
            right.centerY == left.centerY
        }
        
        layout(secondMinutesDigit, firstMinutesDigit) { right, left in
            right.leading == left.trailing
            right.centerY == left.centerY
        }
    }
    
    
    // MARK: Value transformers
    
    private func stringFromDigits(digits: [Int]) -> String {
        if countElements(digits) == 4 {
            let minutesString =  "\(digits[0])\(digits[1]):\(digits[2])\(digits[3])"
            return minutesString
        } else {
            return "00:00"
        }
    }
    
    private func minutesFromString(string: String) -> Int {
        var digitsWithoutColon = string.stringByReplacingOccurrencesOfString(":", withString: "", options: NSStringCompareOptions.allZeros, range: nil)
        if countElements(digitsWithoutColon) == 4 {
            var intArray = digitsWithoutColon.toIntArray()
            return minutesFromDigits(intArray)
        } else {
            return 0
        }
    }
    
    private func minutesFromDigits(digits: [Int]) -> Int {
        var h1 = digits[0] * 600
        var h2 = digits[1] * 60
        var m1 = digits[2] * 10
        var m2 = digits[3]
        let minutes = h1 + h2 + m1 + m2
        return minutes
    }
    
    private func digitsFromMinutes(minutes: Int?) -> [Int] {
        if let m = minutes {
            var firstHour : Int = m / 60 / 10
            var secondHour : Int = m / 60 % 10
            var firstMinute : Int = (m - (firstHour * 600 + secondHour * 60)) / 10
            var secondMinute : Int = (m - (firstHour * 600 + secondHour * 60)) % 10
            
            return [firstHour, secondHour, firstMinute, secondMinute]
        } else {
            return [0,0,0,0]
        }
    }
    
    private func saveTextFromDigit(#atIndex: Int) -> String {
        let digit = digits[min(max(atIndex, 0), 4)]
        if let digitText = digit.text {
            if countElements(digitText) == 1 {
                return digitText
            } else {
                return "0"
            }
        }
        return "0"
    }
    
    


}
