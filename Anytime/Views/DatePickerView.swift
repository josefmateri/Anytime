//
//  DatePickerView.swift
//  Anytime
//
//  Created by Josef Materi on 21.10.14.
//  Copyright (c) 2014 Crush & Lovely. All rights reserved.
//

import UIKit
import Cartography

class DatePickerView: UIView {

    var dateView : DateView!
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */

    override init(frame: CGRect) {
        super.init(frame: frame)
                
        dateView = DateView(frame: CGRectZero)
        self.addSubview(dateView)
        
        layout(dateView) { view in
            view.centerX == view.superview!.centerX
            view.centerY == view.superview!.centerY
        }
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
}
