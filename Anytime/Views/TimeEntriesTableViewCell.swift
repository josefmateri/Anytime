//
//  TimeEntriesTableViewCell.swift
//  Anytime
//
//  Created by Josef Materi on 19.10.14.
//  Copyright (c) 2014 Crush & Lovely. All rights reserved.
//

import UIKit
import Cartography

class TimeEntriesTableViewCell: UITableViewCell {

    var titleLabel : UILabel!
    var subtitleLabel : UILabel!
    var minutesBackgroundView : UIView!
    var minutesLabel : UILabel!
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
            super.init(style: style, reuseIdentifier: reuseIdentifier)
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        // Initialization code
        titleLabel = UILabel(frame: CGRectZero)
        titleLabel.setTranslatesAutoresizingMaskIntoConstraints(false)
        self.contentView.addSubview(titleLabel)
        
        titleLabel.text = "User Interface Design"
        
        var fontDescriptor = UIFontDescriptor.preferredFontDescriptorWithTextStyle(UIFontTextStyleHeadline)
        
        titleLabel.font = UIFont.preferredScaledFontForTextStyle( UIFontTextStyleHeadline, scale: 1)
        
        
        layout(titleLabel) { view in
            view.left == view.superview!.left + 15
            view.top == view.superview!.top + 20
        }
        
        subtitleLabel = UILabel(frame: CGRectZero)
        subtitleLabel.setTranslatesAutoresizingMaskIntoConstraints(false)
        self.contentView.addSubview(subtitleLabel)
      
        subtitleLabel.text = "Anytime App"
        subtitleLabel.font = UIFont.preferredScaledFontForTextStyle( UIFontTextStyleSubheadline, scale: 1)
        
        layout(subtitleLabel, titleLabel, self.contentView) { view, view2, superview in
            view.left == view2.left
            view.top == view2.bottom
            view.bottom == superview.bottom - 20
        }
        
        var minutesBackgroundViewHeight : CGFloat = 60
        minutesBackgroundView = UIView(frame: CGRectMake(0, 0, 44, 44))
        self.contentView .addSubview(minutesBackgroundView)
        minutesBackgroundView.backgroundColor = UIColor.blackColor()
        
        minutesBackgroundView.layer.cornerRadius = minutesBackgroundViewHeight / 2.0
        
        minutesBackgroundView.backgroundColor = UIColor(red: 0.329, green: 0.431, blue: 0.478, alpha: 1)
        
        layout(minutesBackgroundView, self.contentView) { view, superview in
            view.centerY == superview.centerY
            view.right == superview.right - 15
            view.height == Float(minutesBackgroundViewHeight)
            view.width == view.height
        }
        
        minutesLabel = UILabel(frame: CGRectZero)
        minutesBackgroundView.addSubview(minutesLabel)
        
        minutesLabel.textColor = UIColor.whiteColor()
        minutesLabel.font = UIFont(name: "HelveticaNeue-Regular", size: 18)
     
        layout(minutesLabel, minutesBackgroundView) { view, superview in
            view.centerX == superview.centerX
            view.centerY == superview.centerY
            //view.right == superview.right - 15
        }
        
        minutesLabel.text = "00:00"
        
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
