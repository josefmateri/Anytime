//
//  ParallaxHeaderView.swift
//  Anytime
//
//  Created by Josef Materi on 19.10.14.
//  Copyright (c) 2014 Crush & Lovely. All rights reserved.
//

import UIKit
import Cartography

protocol ParallaxHeaderViewDelegate {
    func didChangeFrame(parallaxHeaderView: ParallaxHeaderView, frame: CGRect, percentage: CGFloat)
}

class ParallaxHeaderView: UIView, UIScrollViewDelegate {

    var contentView: UIView!
    var contentTopConstraint: NSLayoutConstraint!
    var delegate : ParallaxHeaderViewDelegate?
    var initialHeight : CGFloat!
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect)
    {
        // Drawing code
    }
    */

    
    convenience init(tableView: UITableView, height: CGFloat) {
        self.init(frame: CGRectMake(0, 0, tableView.bounds.size.width, height))
        attachToTableView(tableView)
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    
        initialHeight = frame.height
        
        contentView = UIView(frame: CGRectZero)
        contentView.backgroundColor = UIColor.blueColor()
        self.addSubview(contentView)
        
        layout(contentView, self) { view, superview in
            view.width == superview.width
            view.centerX == superview.centerX
            view.bottom == superview.bottom
            self.contentTopConstraint = view.top == superview.top - Float(self.initialHeight)
        }
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    func attachToTableView(tableView: UITableView) {
        
        tableView.addSubview(self)
        
        layout(self) { view in
            view.bottom == view.superview!.top
            view.centerX == view.superview!.centerX
            view.width == view.superview!.width
        }
        tableView.contentInset = UIEdgeInsetsMake(initialHeight, 0, 0, 0)
        tableView.contentOffset = CGPointMake(0, -initialHeight)
        tableView.addObserver(self, forKeyPath: "contentOffset", options: NSKeyValueObservingOptions.New, context: nil)

    }
    
    override func observeValueForKeyPath(keyPath: String, ofObject object: AnyObject, change: [NSObject : AnyObject], context: UnsafeMutablePointer<Void>) {
        
        var tableView = object as UITableView

        var contentOffset = tableView.contentOffset
        
        updatePosition(tableView)
        
        var percentage = self.contentView.bounds.size.height / initialHeight
        
        didChangeFrame(self.contentView.bounds, percentage: percentage)
        
        delegate?.didChangeFrame(self, frame: self.contentView.bounds, percentage: percentage)
    }
    
    func updatePosition(scrollView: UIScrollView) {
        self.contentTopConstraint.constant = scrollView.contentOffset.y
    }
    
    func didChangeFrame(frame: CGRect, percentage: CGFloat) {
        
    }
    
    func setViewController(viewController: UIViewController) {
        
        self.contentView.addSubview(viewController.view)
        layout(viewController.view) { view in
            view.center == view.superview!.center
            view.width == view.superview!.width
            view.height == view.superview!.height
        }
    }
    
}
