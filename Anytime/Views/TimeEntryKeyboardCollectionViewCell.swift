//
//  TimeEntryKeyboardCollectionViewCell.swift
//  Anytime
//
//  Created by Josef Materi on 23.10.14.
//  Copyright (c) 2014 Crush & Lovely. All rights reserved.
//

import UIKit
import Cartography

class TimeEntryKeyboardCollectionViewCell: UICollectionViewCell {

    var titleLabel : UILabel!
    var imageView : UIImageView!
    
    override var selected: Bool {
        didSet {
            if (self.selected) {
                self.selectedBackgroundView.backgroundColor = UIColor.clearColor()
            } else {
                self.selectedBackgroundView.backgroundColor = UIColor.clearColor()
            }
        }
    }
    
    override var highlighted: Bool {
        didSet {
            if (self.highlighted) {
                self.selectedBackgroundView.backgroundColor = UIColor(white: 0, alpha: 0.3)
                self.layer.borderColor = UIColor.whiteColor().colorWithAlphaComponent(0.15).CGColor
            } else {
                self.selectedBackgroundView.backgroundColor = UIColor.clearColor()
                self.layer.borderColor = UIColor.whiteColor().colorWithAlphaComponent(0.15).CGColor
            }
        }
    }

    
    override init(frame : CGRect) {
        super.init(frame: frame)
        
        self.clipsToBounds = true
        
        titleLabel = UILabel(frame: CGRectZero)
        self.addSubview(titleLabel)
        
        titleLabel.textColor = UIColor.whiteColor()
        titleLabel.font = UIFont(name: "HelveticaNeue", size: 24)
        
        layout(titleLabel) { titleLabel in
            titleLabel.centerX == titleLabel.superview!.centerX
            titleLabel.centerY == titleLabel.superview!.centerY
        }
        
        imageView = UIImageView(frame: CGRectZero)
        imageView.contentMode = UIViewContentMode.Center
        imageView.tintColor = UIColor.whiteColor().colorWithAlphaComponent(0.25)
        self.addSubview(imageView)
        
        layout(imageView) { titleLabel in
            titleLabel.centerX == titleLabel.superview!.centerX
            titleLabel.centerY == titleLabel.superview!.centerY
            titleLabel.height == titleLabel.superview!.height
            titleLabel.width == titleLabel.superview!.width
        }
     
        self.selectedBackgroundView = UIView(frame: CGRectZero)
    }
    
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
