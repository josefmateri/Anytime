//
//  DateView.swift
//  Anytime
//
//  Created by Josef Materi on 20.10.14.
//  Copyright (c) 2014 Crush & Lovely. All rights reserved.
//

import UIKit
import Cartography

class DateView: UIView {

    var dayLabel: UILabel!
    var yearLabel: UILabel!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        
        
        dayLabel = UILabel(frame: CGRectMake(0,50,100,20))
        dayLabel.textColor = UIColor.whiteColor()
        dayLabel.text = "03. Oktober"
        dayLabel.font = UIFont(name: "HelveticaNeue", size: 24)
        dayLabel.setTranslatesAutoresizingMaskIntoConstraints(false)
        
        self.addSubview(dayLabel)
        
       layout(dayLabel, self) { view, superview in
           view.left == superview.left
           view.right == superview.right
            view.centerX == superview.centerX
            view.top == superview.top
       }
       
       yearLabel = UILabel(frame: CGRectZero)
       yearLabel.textColor = UIColor.whiteColor()
       yearLabel.text = "2014"
       yearLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 24)
       yearLabel.setTranslatesAutoresizingMaskIntoConstraints(false)
       self.addSubview(yearLabel)
        
        layout(yearLabel, self) { view, superview in
            view.centerX == superview.centerX
            superview.bottom == view.bottom
        }
        
        layout(yearLabel, dayLabel) { view, superview in
            view.top == superview.bottom - 5
            view.centerX == superview.centerX
        }
        

    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect)
    {
        // Drawing code
    }
    */

}
