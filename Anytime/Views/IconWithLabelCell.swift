//
//  LabeledIconCell.swift
//  Anytime
//
//  Created by Josef Materi on 24.10.14.
//  Copyright (c) 2014 Crush & Lovely. All rights reserved.
//

import UIKit
import Cartography

class IconWithLabelCell: UICollectionViewCell {
    
    var titleLabel : UILabel!
    var imageView : UIImageView!
    
    override var selected: Bool {
        didSet {
            if (self.selected) {
                imageView.tintColor = UIColor.whiteColor()
                titleLabel.textColor = UIColor.whiteColor()
            } else {
                titleLabel.textColor = UIColor(white: 1, alpha: 0.5)
                imageView.tintColor = UIColor(white: 1, alpha: 0.5)
            }
        }
    }
    
    override init(frame : CGRect) {
        super.init(frame: frame)
        
        self.clipsToBounds = true
        
        imageView = UIImageView(frame: CGRectZero)
        imageView.contentMode = UIViewContentMode.Center
        imageView.tintColor = UIColor.whiteColor().colorWithAlphaComponent(0.75)
        self.addSubview(imageView)
        
        layout(imageView) { titleLabel in
            titleLabel.centerX == titleLabel.superview!.centerX
            titleLabel.centerY == titleLabel.superview!.centerY - 10
            titleLabel.height == titleLabel.superview!.height
            titleLabel.width == titleLabel.superview!.width
        }
        
        titleLabel = UILabel(frame: CGRectZero)
        self.addSubview(titleLabel)
        
        titleLabel.textColor = UIColor.whiteColor()
        titleLabel.font = UIFont(name: "HelveticaNeue-Light", size: 18)
        
        layout(titleLabel) { titleLabel in
            titleLabel.centerX == titleLabel.superview!.centerX
            titleLabel.centerY == titleLabel.superview!.centerY + 20
        }
        
        self.selectedBackgroundView = UIView(frame: CGRectZero)
        
        titleLabel.textColor = UIColor(white: 1, alpha: 0.5)
        imageView.tintColor = UIColor(white: 1, alpha: 0.5)
    }
    
    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
