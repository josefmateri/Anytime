//
//  KeyboardModeView.swift
//  Anytime
//
//  Created by Josef Materi on 24.10.14.
//  Copyright (c) 2014 Crush & Lovely. All rights reserved.
//

import UIKit
import Cartography

enum KeyboardMode {
    case Digits
    case Time
    case History
}

protocol KeyboardModeViewDelegate {
    func keyboardModeView(keyboardModeView: KeyboardModeView, didSwitchMode mode: KeyboardMode)
}

class KeyboardModeView: UIView, UICollectionViewDelegate, UICollectionViewDataSource {

    var collectionView: UICollectionView!

    var mode : KeyboardMode = .Digits

    var delegate : KeyboardModeViewDelegate?
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */


    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        self.clipsToBounds = true
        
        var flowLayout = UICollectionViewFlowLayout()
        flowLayout.itemSize = CGSizeMake(65, 65)
        flowLayout.minimumInteritemSpacing = 15
        flowLayout.minimumLineSpacing = 20
        
        collectionView = UICollectionView(frame: CGRectZero, collectionViewLayout: flowLayout)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.registerClass(IconWithLabelCell.classForCoder(), forCellWithReuseIdentifier: "cell")
        collectionView.backgroundColor = UIColor.clearColor()
        collectionView.delaysContentTouches = false
        
        self.addSubview(collectionView)


        collectionView.transform = CGAffineTransformMakeScale(0.75, 0.75)
        
        layout(collectionView) { view in
            view.width == view.superview!.width
            view.height == 320
            view.center == view.superview!.center
        }
        
        collectionView .reloadData()
        
        collectionView.selectItemAtIndexPath(NSIndexPath(forItem: 0, inSection: 0), animated: true, scrollPosition: UICollectionViewScrollPosition.Top)
        
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        var collectionViewCell = collectionView.dequeueReusableCellWithReuseIdentifier("cell", forIndexPath: indexPath) as IconWithLabelCell
        
        collectionViewCell.hidden = false
        collectionViewCell.imageView.image = nil
        
        if (indexPath.row == 0) {
            collectionViewCell.imageView.image = UIImage(named: "icon_digits")
            collectionViewCell.titleLabel.text = "Ziffern"
        }

        if (indexPath.row == 1) {
            collectionViewCell.imageView.image = UIImage(named: "icon_time")
            collectionViewCell.titleLabel.text = "Zeiten"
        }
        
        if (indexPath.row == 2) {
            collectionViewCell.imageView.image = UIImage(named: "icon_history")
            collectionViewCell.titleLabel.text = "Verlauf"
        }
        

        return collectionViewCell
    }
    

    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 3
    }
    
    func collectionView(collectionView: UICollectionView, didSelectItemAtIndexPath indexPath: NSIndexPath) {
        var cell = collectionView.cellForItemAtIndexPath(indexPath)
        cell?.selected = true
        
        self.mode = keyboardModeForIndexPath(indexPath)
        
        delegate?.keyboardModeView(self, didSwitchMode: self.mode)
    }

    func keyboardModeForIndexPath(indexPath: NSIndexPath) -> KeyboardMode {
        if (indexPath.row == 0) {
            return .Digits
        }
        if (indexPath.row == 1) {
            return .Time
        }
        if (indexPath.row == 2) {
            return .History
        }
        return .Digits
    }

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
