//
//  TimeEntriesTableHeaderView.swift
//  Anytime
//
//  Created by Josef Materi on 19.10.14.
//  Copyright (c) 2014 Crush & Lovely. All rights reserved.
//

import UIKit
import Cartography

class TimeEntriesTableHeaderView: ParallaxHeaderView {

    var yOffsetConstraint : NSLayoutConstraint!

    var progressView : MDRadialProgressView!
    var progressTheme : MDRadialProgressTheme!
    
    var contentInsets : UIEdgeInsets = UIEdgeInsetsMake(15, 0, 0, 0) {
        didSet {
            //self.yOffsetConstraint.constant = contentInsets.top
        }
    }
    
    override var tintColor: UIColor! {
        didSet {
         //   progressTheme.completedColor = tintColor
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
//
    }

    override func didChangeFrame(frame: CGRect, percentage: CGFloat) {
        var filteredPercentage = min(1, percentage)
//        progressView.alpha = 1 - ((1 - filteredPercentage) * 1.0)
//        progressView.transform = CGAffineTransformMakeScale(filteredPercentage, filteredPercentage)
        //self.yOffsetConstraint.constant = self.contentInsets.top - (1 - filteredPercentage) * self.contentInsets.top * 2
    }
    

    
    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}
