//
//  AnytimeNavigationBar.swift
//  Anytime
//
//  Created by Josef Materi on 19.10.14.
//  Copyright (c) 2014 Crush & Lovely. All rights reserved.
//

import UIKit
import Cartography

protocol AnytimeNavigationBarDelegate {
    func navigationBar(navigationBar: AnytimeNavigationBar, didSelectTitleView titleView: DateView)
}

class AnytimeNavigationBar: UIView {

    var delegate : AnytimeNavigationBarDelegate?
    
    var backgroundView : UIVisualEffectView!
    var navigationBar : UINavigationBar!
    var shadowView : UIView!
    var titleView : DateView!
    
    var rightBarButtonItem : UIBarButtonItem! {
        didSet {
           self.navigationBar.topItem?.rightBarButtonItem = self.rightBarButtonItem
        }
    }
    var leftBarButtonItem : UIBarButtonItem! {
        didSet {
            self.navigationBar.topItem?.leftBarButtonItem = self.leftBarButtonItem
        }
    }

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect)
    {
        // Drawing code
    }
    */

     required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    
        self.setTranslatesAutoresizingMaskIntoConstraints(false)
        self.clipsToBounds = true
        
        navigationBar = UINavigationBar(frame: frame)
        
        navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
        navigationBar.translucent = true
        navigationBar.backgroundColor = UIColor.clearColor()
        navigationBar.barStyle = UIBarStyle.BlackTranslucent
        navigationBar.shadowImage = UIImage()

        navigationBar.pushNavigationItem(UINavigationItem(), animated: false)
        
        self.addSubview(navigationBar)
        
        var offset = UIApplication.sharedApplication().statusBarFrame.size.height * 1.5
        
        layout(navigationBar, self) { view, superview in
            view.centerY == superview.centerY + 10
            view.centerX == superview.centerX
            view.width == superview.width
        }
        

        
//        shadowView = UIView(frame: CGRectZero)
//        shadowView.backgroundColor = UIColor(white: 1, alpha: 0.05)
//        self.addSubview(shadowView)
//        
//        layout(shadowView, self) { view, superview in
//            view.height == 1
//            view.bottom == superview.bottom
//            view.width == superview.width
//            view.centerX == superview.centerX
//        }
        
        var editButton = UIBarButtonItem(image: UIImage(named: "icon_edit"), style: UIBarButtonItemStyle.Bordered, target: nil, action: nil)
        self.rightBarButtonItem = editButton
        
        
        var listButton = UIBarButtonItem(image: UIImage(named: "icon_list"), style: UIBarButtonItemStyle.Bordered, target: nil, action: nil)
        self.leftBarButtonItem = editButton
        navigationBar.tintColor = UIColor.whiteColor()
        
        
//        var flatListButton = VBFPopFlatButton(frame: CGRectMake(0, 0, 20, 20), buttonType: FlatButtonType.buttonMenuType, buttonStyle: FlatButtonStyle.buttonRoundedStyle, animateToInitialState: true)
//        flatListButton.lineThickness = 2;
//        flatListButton.lineRadius = 2
//        flatListButton.roundBackgroundColor = UIColor.whiteColor()
//        flatListButton.tintColor = UIColor(red: 0.282, green: 0.333, blue: 0.388, alpha: 1)
//
//        var listButton = UIBarButtonItem(customView: flatListButton)
//        
        
        titleView = DateView(frame: CGRectMake(0, 0, 100, 100))
        self.addSubview(titleView)
        titleView.backgroundColor = UIColor.clearColor()
        titleView.setTranslatesAutoresizingMaskIntoConstraints(false)
        
        layout(titleView, navigationBar) { view, superview in
            view.centerY == superview.centerY
            view.centerX == superview.centerX
        }
        
        var tapGesture = UITapGestureRecognizer(target: self, action: "didTapTitle:")
        self.addGestureRecognizer(tapGesture)
        
    }
    
    func didTapTitle(gesture: UITapGestureRecognizer) {
        delegate?.navigationBar(self, didSelectTitleView: titleView)
    }
}




