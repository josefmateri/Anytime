//
//  BarButtonItemExtension.swift
//  Anytime
//
//  Created by Josef Materi on 22.10.14.
//  Copyright (c) 2014 Crush & Lovely. All rights reserved.
//

import UIKit

private var xoAssociationKey: UInt8 = 0

extension UIBarButtonItem : UIGestureRecognizerDelegate {

    var backedButton: VBFPopFlatButton! {
        get {
            return objc_getAssociatedObject(self, &xoAssociationKey) as? VBFPopFlatButton
        }
        set(newValue) {
            objc_setAssociatedObject(self, &xoAssociationKey, newValue, UInt(OBJC_ASSOCIATION_RETAIN))
        }
    }
    
    convenience init(frame: CGRect, buttonType: FlatButtonType, buttonStyle: FlatButtonStyle, animateToInitialState: Bool) {
        
        var closeButton = VBFPopFlatButton(frame: frame, buttonType: buttonType, buttonStyle: buttonStyle, animateToInitialState: animateToInitialState)
        
        self.init(customView: closeButton)

        self.backedButton = closeButton as VBFPopFlatButton
        
    }
    
    func animateToType(buttonType: FlatButtonType) {
        self.backedButton.animateToType(buttonType)
    }
    
    func setTintColor(tintColor: UIColor!, forState: UIControlState) {
        if let button = self.backedButton {
            button.setTintColor(tintColor, forState: forState)
        }
    }
    
    func addTarget(target: AnyObject?, action: Selector, forControlEvents: UIControlEvents) {
        if let b = backedButton {
            b.addTarget(target, action: action, forControlEvents: forControlEvents)
        }
    }
    
}
