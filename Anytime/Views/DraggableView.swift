//
//  DraggableView.swift
//  Anytime
//
//  Created by Josef Materi on 26.10.14.
//  Copyright (c) 2014 Crush & Lovely. All rights reserved.
//

import UIKit

class DraggableView: UIView {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    override func hitTest(point: CGPoint, withEvent event: UIEvent?) -> UIView? {
        let superviewPoint = convertPoint(point, toView: superview)
        var superviewLayer = superview?.layer
        let point = layer.presentationLayer().convertPoint(superviewPoint, fromLayer: superviewLayer)
        return super.hitTest(point, withEvent: event)
    }
    
}
