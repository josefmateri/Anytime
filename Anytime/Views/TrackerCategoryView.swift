//
//  TrackerCategoryView.swift
//  Anytime
//
//  Created by Josef Materi on 21.10.14.
//  Copyright (c) 2014 Crush & Lovely. All rights reserved.
//

import UIKit
import Cartography

class TrackerCategoryView: UIView {

    var mainCategoryLabel: UILabel!
    var subCategoryLabel: UILabel!
    
    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
    }
    */
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        mainCategoryLabel = UILabel(frame: CGRectZero)
        mainCategoryLabel.setTranslatesAutoresizingMaskIntoConstraints(false)
        mainCategoryLabel.textColor = UIColor.whiteColor()
        mainCategoryLabel.font = UIFont.preferredScaledFontForTextStyle(UIFontTextStyleHeadline, scale: 1.5)
        addSubview(mainCategoryLabel)

        subCategoryLabel = UILabel(frame: CGRectZero)
        subCategoryLabel.textColor = UIColor.whiteColor()
        subCategoryLabel.setTranslatesAutoresizingMaskIntoConstraints(false)
        subCategoryLabel.font = UIFont.preferredScaledFontForTextStyle(UIFontTextStyleSubheadline, scale: 1.5)
        addSubview(subCategoryLabel)
        
//        var constraints = NSLayoutConstraint.constraintsWithVisualFormat("V:|-(20)-[mainCategoryLabel]-(0)-[subCategoryLabel]|", options: NSLayoutFormatOptions.AlignAllLeft, metrics: nil, views: ["mainCategoryLabel": mainCategoryLabel, "subCategoryLabel": subCategoryLabel])
//        
//        var b = NSLayoutConstraint(item: subCategoryLabel, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: mainCategoryLabel, attribute: NSLayoutAttribute.Bottom, multiplier: 1, constant: 0)
//        addConstraint(b)
        
//        addConstraints(constraints)
  
        layout(mainCategoryLabel) { view in
            view.leading == view.superview!.leading + 40
            view.top == view.superview!.top + 20
        }
        
        layout(subCategoryLabel, mainCategoryLabel) { view, mainCategoryLabel in
            view.leading == view.superview!.leading + 40
            view.top == mainCategoryLabel.bottom
        }
        
    }
    

    required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    

}
