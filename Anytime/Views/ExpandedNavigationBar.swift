//
//  ExpandedNavigationBar.swift
//  Anytime
//
//  Created by Josef Materi on 19.10.14.
//  Copyright (c) 2014 Crush & Lovely. All rights reserved.
//

import UIKit

class ExpandedNavigationBar: UINavigationBar {

    /*
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect)
    {
        // Drawing code
    }
    */
    
    override func sizeThatFits(size: CGSize) -> CGSize {
        var size = super.sizeThatFits(size)
        return CGSizeMake(size.width, 64)
    }

    override func layoutSubviews() {
        super.layoutSubviews()
    
        for view  in self.subviews {
            
            var thisView = view as UIView
            
            var center : CGPoint = thisView.center
            center.y = self.center.y - UIApplication.sharedApplication().statusBarFrame.height * 1.5
            thisView.center = center
        
        }
    
        var view = UIView()
        view.center = CGPointMake(10, 10)
    }
    
    
}
