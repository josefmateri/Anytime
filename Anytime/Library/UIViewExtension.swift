//
//  UIViewExtension.swift
//  Anytime
//
//  Created by Josef Materi on 24.10.14.
//  Copyright (c) 2014 Crush & Lovely. All rights reserved.
//

import Foundation

private var xoAssociationKey: UInt8 = 0

class AnimationDidFinishWrapper : NSObject {
    
    var closure : ((finished: Bool) -> Void)?
    
    init(closure: ((finished: Bool) -> Void)?) {
        super.init()
        self.closure = closure
    }
    
}

extension UIView {
    
    var closure: AnimationDidFinishWrapper? {
        get {
            return objc_getAssociatedObject(self, &xoAssociationKey) as AnimationDidFinishWrapper?
        }
        set(newValue) {
            objc_setAssociatedObject(self, &xoAssociationKey, newValue, UInt(OBJC_ASSOCIATION_RETAIN))
        }
    }
    
    func blinkWithInterval(interval: NSTimeInterval) {
        
        var anim = CABasicAnimation()
        anim.keyPath = "opacity"
        anim.fromValue = 1
        anim.duration = interval
        anim.toValue = 0.25
        anim.repeatCount = 9999
        anim.autoreverses = true
        anim.delegate = self
        anim.fillMode = kCAFillModeForwards
        anim.removedOnCompletion = false
        self.layer.addAnimation(anim, forKey: "opcity")
        
    }
    
    func stopBlinking() {
        
        self.layer.removeAnimationForKey("opcity")
    
    }
    
    func shake(duration: NSTimeInterval = 0.6,  finished: (finished: Bool) -> Void)  {
        
        self.closure = AnimationDidFinishWrapper(closure: finished)

        var animation = CAKeyframeAnimation(keyPath: "transform.translation.x")
        animation.timingFunction = CAMediaTimingFunction(name: "linear")
        animation.duration = duration
        animation.values = [ (-5), (5), (-5), (5), (-3), (3), (-1), (1), (0) ];
        animation.delegate = self
        self.layer.addAnimation(animation, forKey: "shake")

    }
    
    func stopShake() {
        self.layer.removeAnimationForKey("shake")
    }
    

    public override func animationDidStop(anim: CAAnimation!, finished flag: Bool) {
        if let c = self.closure {
            if let x = c.closure {
                x(finished: flag)
            }
        }

    }
    
}