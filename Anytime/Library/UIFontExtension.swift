//
//  UIFontExtension.swift
//  Anytime
//
//  Created by Josef Materi on 19.10.14.
//  Copyright (c) 2014 Crush & Lovely. All rights reserved.
//

import UIKit

extension UIFont {
    class func preferredScaledFontForTextStyle(style: String, scale: CGFloat) -> UIFont! {
    
        var fontDescriptor = UIFontDescriptor.preferredFontDescriptorWithTextStyle(style)
        
        return UIFont(descriptor: fontDescriptor, size: fontDescriptor.pointSize * scale)
        
    }
}
