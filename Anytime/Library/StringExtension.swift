//
//  StringExtension.swift
//  Anytime
//
//  Created by Josef Materi on 25.10.14.
//  Copyright (c) 2014 Crush & Lovely. All rights reserved.
//

import Foundation

extension String {
    func toIntArray() -> [Int] {
        
        var array : [Int] = []
        for c in self {
            var string2 = String(c)
            array.append(string2.toInt()!)
        }
        return array
    }

}