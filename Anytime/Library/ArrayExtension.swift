//
//  ArrayExtension.swift
//  Anytime
//
//  Created by Josef Materi on 25.10.14.
//  Copyright (c) 2014 Crush & Lovely. All rights reserved.
//

import Foundation

extension Array {
    
    func atIndex(index: Int) -> T! {
        return self[min(index, countElements(self) - 1)]
    }
    
}