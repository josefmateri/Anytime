//
//  Constants.h
//  Anytime
//
//  Created by Josef Materi on 10/19/14
//  Copyright (c) 2014 Josef Materi. All rights reserved.
//

#import <Foundation/Foundation.h>

// Use this file to declare constants, especially those that change
// between staging and production.

// For example:
//extern NSString * const APIKey;
//extern NSURL * const APIRoot;