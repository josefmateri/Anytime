//
//  AppDelegate.h
//  Anytime
//
//  Created by Josef Materi on 10/19/14
//  Copyright (c) 2014 Josef Materi. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
