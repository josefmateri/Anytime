//
//  ContainerViewController.swift
//  Anytime
//
//  Created by Josef Materi on 19.10.14.
//  Copyright (c) 2014 Crush & Lovely. All rights reserved.
//

import UIKit
import Cartography

enum StatisticMode {
    case Collapsed
    case Expanded
}

class ContainerViewController: UIViewController, ParallaxHeaderViewDelegate, TableViewControllerDelegate {

    var backgroundImageView : UIImageView!
    
    var statisticMode : StatisticMode = .Collapsed
    
    var timeEntriesTableViewController : TimeEntriesTableViewController!
    var trackerViewController : TrackerViewController!
    
    var navigationBar : AnytimeNavigationBar!
    var navigationBarHeightConstraint : NSLayoutConstraint!
    
    var initialPanPoint: CGPoint!
    
    var bottomPositionConstraint: NSLayoutConstraint!
    
    var parallaxHeaderView: TimeEntriesTableHeaderView!
    
    var timeEntriesTableViewControllerTopConstraint : NSLayoutConstraint!
    
    var statsViewController : TimeEntriesStatisticsViewController!
    
    override func viewDidLoad() {
        super.viewDidLoad()

//       backgroundImageView = UIImageView(image: UIImage(named: "background_dark"))
//       backgroundImageView.contentMode = UIViewContentMode.ScaleToFill
//       //self.view .addSubview(backgroundImageView)
//       backgroundImageView.setTranslatesAutoresizingMaskIntoConstraints(false)
//       layout(backgroundImageView) { view in
//           view.size   == view.superview!.size
//           view.center == view.superview!.center
//            view.bottom == view.superview!.bottom
//           view.top == view.superview!.top
//       }
//     
        //self.view.backgroundColor = UIColor(red: 0.251, green: 0.329, blue: 0.698, alpha: 1)
        
//      self.navigationController?.navigationBar.setBackgroundImage(UIImage(), forBarMetrics: UIBarMetrics.Default)
//      //self.navigationController?.navigationBar.shadowImage = UIImage()
//      self.navigationController?.navigationBar.translucent = true
//        self.navigationController?.navigationBar.backgroundColor = UIColor.clearColor()
//      self.navigationController?.navigationBar.barStyle = UIBarStyle.BlackTranslucent
//    
//       self.navigationItem.rightBarButtonItem = self.editButtonItem()
        
        
        navigationBar = AnytimeNavigationBar(frame: CGRectMake(0, 0, 0, 0))
        navigationBar.backgroundColor = UIColor(red: 0.376, green: 0.49, blue: 0.545, alpha: 1)
     //   navigationBar.backgroundColor = UIColor.whiteColor()
        
        self.view .addSubview(navigationBar)
        
        layout(navigationBar, self.view) { view, superview in
            view.width == superview.width
            view.top == superview.top
            view.centerX == superview.centerX
            self.navigationBarHeightConstraint = view.height == 100
        }
        
        timeEntriesTableViewController = TimeEntriesTableViewController(style: UITableViewStyle.Plain)
        timeEntriesTableViewController.delegate = self
        
        self.addChildViewController(timeEntriesTableViewController)
       self.view .addSubview(timeEntriesTableViewController.view)
       timeEntriesTableViewController.view.setTranslatesAutoresizingMaskIntoConstraints(false)
       
       timeEntriesTableViewController.tableView.contentInset = UIEdgeInsetsMake(0, 0, 0, 0)
       
       layout(timeEntriesTableViewController.view, self.view) { view, view2 in
       
           self.timeEntriesTableViewControllerTopConstraint = view.top == view2.top
           view.centerX == view.superview!.centerX
           view.width == view.superview!.width
           view.bottom == view.superview!.bottom
       }

        parallaxHeaderView = TimeEntriesTableHeaderView(tableView: timeEntriesTableViewController.tableView, height: 224)
        parallaxHeaderView.contentView.backgroundColor = UIColor(red: 0.376, green: 0.49, blue: 0.545, alpha: 1)
        parallaxHeaderView.delegate = self
        parallaxHeaderView.tintColor = UIColor.whiteColor()
        self.navigationController?.navigationBarHidden = true
        
        statsViewController = TimeEntriesStatisticsViewController()
        parallaxHeaderView.setViewController(statsViewController)
        
        // Do any additional setup after loading the view.
        
        trackerViewController = TrackerViewController(nibName: nil, bundle: nil)
        self.addChildViewController(trackerViewController)
        
        self.view.addSubview(trackerViewController.view)
        
        layout(trackerViewController.view, self.view) { view, superview in
            view.centerX == superview.centerX
            view.width == superview.width
            view.height == superview.height
            self.bottomPositionConstraint = view.top == superview.bottom - 88
        }

        trackerViewController.bottomPositionConstraint = self.bottomPositionConstraint
        
        navigationBar.leftBarButtonItem = UIBarButtonItem(frame: CGRectMake(0, 0, 25, 25), buttonType: FlatButtonType.buttonMenuType, buttonStyle: FlatButtonStyle.buttonPlainStyle, animateToInitialState: false)
        
        self.view.bringSubviewToFront(navigationBar)
        self.view.bringSubviewToFront(trackerViewController.view)
    }


    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue!, sender: AnyObject!) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

extension ContainerViewController {
    
    func didChangeFrame(parallaxHeaderView: ParallaxHeaderView, frame: CGRect, percentage: CGFloat) {
        navigationBarHeightConstraint.constant = max(min(100 * percentage, 100), 64)
        var scale = min(max(percentage, 0.64), 1)
        navigationBar.titleView.transform = CGAffineTransformMakeScale(scale, scale)
        
    }
}

extension ContainerViewController {
    
    func tableViewControllerDidEndDragging(tableViewController: UITableViewController, scrollView: UIScrollView, willDecelerate: Bool) {

        if statisticMode == .Collapsed {
            if scrollView.contentOffset.y > -scrollView.contentInset.top {
                return
            }
        }
        
        var goingDown = false
        
        if scrollView.contentOffset.y < -(scrollView.contentInset.top) {

            goingDown = true
            scrollView.contentInset = UIEdgeInsetsMake(600, 0, 0, 0)
            statisticMode = .Expanded
        } else {
            statisticMode = .Collapsed
        }
        
        
        UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: UIViewAnimationOptions.AllowUserInteraction, animations: { () -> Void in
            
            
            if goingDown {

                scrollView.contentOffset = CGPointMake(0, -600)
                self.view.layoutIfNeeded()
            
                self.statsViewController.setScale(2)
                
            } else {
                scrollView.contentOffset = CGPointMake(0, -224)
                self.view.layoutIfNeeded()
                
                self.statsViewController.setScale(1)

            }
            
            
            }) { (finished: Bool) -> Void in
                if goingDown {
                } else {
                    scrollView.contentInset = UIEdgeInsetsMake(224, 0, 0, 0)
                }
        }
    }
    
    func tableViewControllerWillEndDragging(tableViewController: UITableViewController, scrollView: UIScrollView, withVelocity: CGPoint,
        targetContentOffset: UnsafeMutablePointer<CGPoint>) {
            
            
            if statisticMode == .Collapsed {
              
                if scrollView.contentOffset.y < -scrollView.contentInset.top {
                        targetContentOffset.memory.y = scrollView.contentOffset.y
                }
                
            }
            
            if statisticMode == .Expanded {
                targetContentOffset.memory.y = scrollView.contentOffset.y
            }

    }
    
}