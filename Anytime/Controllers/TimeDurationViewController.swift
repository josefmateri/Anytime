//
//  TimeDurationViewController.swift
//  Anytime
//
//  Created by Josef Materi on 30.10.14.
//  Copyright (c) 2014 Crush & Lovely. All rights reserved.
//

import UIKit
import Cartography

enum TimeDurationEditingMode {
    case Editing
    case NotEditing
}

protocol TimeDurationViewControllerDelegate {
    func timeDurationViewController(timeDurationViewController: TimeDurationViewController, didSwitchMode mode: TimeDurationEditingMode)
}

class TimeDurationViewController: UIViewController , TimeEntryKeyboardDelegate, DigitsViewDelegate {

    var digitsView : DigitsView!
    var delegate : TimeDurationViewControllerDelegate?
    
    var digitsBottomLineTopConstraint : NSLayoutConstraint!
    var digitsViewHeightConstraint : NSLayoutConstraint!
    var digitsViewCenterConstraint : NSLayoutConstraint!
    
    var closedTrackerDigitsScale : CGFloat = 0.35
    var closedTrackerHeight : CGFloat = 88
    var openedTrackerHeight : CGFloat = 200
    
    var heightConstraint : NSLayoutConstraint!
    
    var keyboard : TimeEntryKeyboard!
    var timeEntryKeyboardHeightConstraint : NSLayoutConstraint!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.clipsToBounds = false
                
        digitsView = DigitsView(frame: CGRectZero)
        digitsView.delegate = self
        self.view .addSubview(digitsView)
        
        
        layout(digitsView, self.view) { view, dateViewLine in
            view.centerX == view.superview!.centerX
            view.centerY == view.superview!.centerY
            self.digitsViewCenterConstraint = view.centerY == view.superview!.top + Float(self.closedTrackerHeight / 2)
            view.width == view.superview!.width
            self.digitsViewHeightConstraint = view.height == Float(self.closedTrackerHeight)
        }
        
        self.heightConstraint = NSLayoutConstraint(item: self.view, attribute: NSLayoutAttribute.Height, relatedBy: NSLayoutRelation.Equal, toItem: nil, attribute: NSLayoutAttribute.NotAnAttribute, multiplier: 1, constant: self.closedTrackerHeight)
        
        self.view.addConstraint(self.heightConstraint)

        
        keyboard = TimeEntryKeyboard(frame: CGRectZero)
        keyboard.delegate = self
        self.view .addSubview(keyboard!)
        
        layout(keyboard!, digitsView) { keyboard, upperView in
            keyboard.top == upperView.bottom
            keyboard.centerX == keyboard.superview!.centerX
            keyboard.width == keyboard.superview!.width
            self.timeEntryKeyboardHeightConstraint = keyboard.height == Float(self.keyboard!.heightForState(KeyboardState.Closed))
        }

        
        // Do any additional setup after loading the view.
    }
    

    
    func setKeyboardScale(scale: CGFloat) {
        self.keyboard?.collectionView.transform = CGAffineTransformMakeScale(scale, scale)
        self.keyboard?.keyboardModeView.collectionView.transform = CGAffineTransformMakeScale(scale, scale)
    }

    func slideDigitsOut() {
        setScale(0)
        self.heightConstraint.constant = 0
        self.digitsViewCenterConstraint.constant = -(self.digitsView.bounds.size.height * closedTrackerDigitsScale) / 2
    }
    
    
    func setScale(scale: CGFloat) {
        
        self.digitsView.layer.transform = CATransform3DMakeScale(closedTrackerDigitsScale + ((1 - closedTrackerDigitsScale) * scale), closedTrackerDigitsScale + ((1 - closedTrackerDigitsScale) * scale), 1)
        
        self.digitsViewHeightConstraint.constant = closedTrackerHeight + (openedTrackerHeight - closedTrackerHeight) * scale
        self.digitsViewCenterConstraint.constant = closedTrackerHeight / 2 + (openedTrackerHeight - closedTrackerHeight) / 2 * scale - (40 * scale)
        
        self.heightConstraint.constant = self.digitsViewHeightConstraint.constant - (40 * scale)
        
    }
    
    func setDigitsViewScale(scale: CGFloat) {
        self.digitsView.layer.transform = CATransform3DMakeScale(closedTrackerDigitsScale + ((1 - closedTrackerDigitsScale) * scale), closedTrackerDigitsScale + ((1 - closedTrackerDigitsScale) * scale), 1)
    }

    func transitionToMode(mode: TimeDurationEditingMode) {
        
        if mode == .NotEditing {
            self.digitsView.unselectAllDigits()
        }
        
        self.delegate?.timeDurationViewController(self, didSwitchMode: mode)
        
        UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 2.5, options: UIViewAnimationOptions.AllowUserInteraction , animations: { () -> Void in
            
            self.setDigitsViewScale(1)

            if mode == .Editing {
                
                self.timeEntryKeyboardHeightConstraint.constant = self.keyboard!.heightForState(.Open)
                self.heightConstraint.constant = 105 + self.keyboard!.heightForState(.Open)
                self.digitsViewHeightConstraint.constant = 141
                self.digitsViewCenterConstraint.constant = 35
                self.setDigitsViewScale(0.75)
                self.setKeyboardScale(1)

            } else {
                self.setKeyboardScale(0.75)
                self.timeEntryKeyboardHeightConstraint.constant = self.keyboard!.heightForState(.Closed)
                self.setScale(1)
            }
            
            self.view.layoutIfNeeded()

            self.view.superview!.layoutIfNeeded()
            
            }) { (finished: Bool) -> Void in
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}

extension TimeDurationViewController {
    func digitsView(digitsView: DigitsView, didSelectDigitAtIndex index: Int) {
        transitionToMode(.Editing)
    }
}

extension TimeDurationViewController {
    
    func timeEntryKeyboard(keyboard: TimeEntryKeyboard, didSelectDigit: Int) {
        if (self.digitsView.setDigitAtSelectedIndex(didSelectDigit) == true) {
            if (self.digitsView.jumpToNextDigit() == false) {

            }
        }
        //updateTimeRangeConfiguration()
    }
    
    func timeEntryKeyboard(keyboard: TimeEntryKeyboard, didSwitch mode: KeyboardMode) {
        prepareDigitForMode(mode)
    }
    
    func prepareDigitForMode(mode: KeyboardMode) {

    }
    
}
