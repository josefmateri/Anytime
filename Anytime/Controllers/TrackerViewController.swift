//
//  TrackerViewController.swift
//  Anytime
//
//  Created by Josef Materi on 20.10.14.
//  Copyright (c) 2014 Crush & Lovely. All rights reserved.
//

import UIKit
import Cartography

enum TrackerMode {
    case NotEditing
    case EditingDuration
    case EditingTimeRange
}

class TrackerViewController: UIViewController, UIGestureRecognizerDelegate, TimeRangeViewContollerDelegate, TimeDurationViewControllerDelegate, AnytimeNavigationBarDelegate {

    var mode : TrackerMode = .NotEditing
    
    var backgroundView: UIVisualEffectView!
    weak var bottomPositionConstraint: NSLayoutConstraint!
    var initialPanPoint : CGPoint!
    var addButton : UIButton!
    
    var timeDurationViewController : TimeDurationViewController!
    var timeDurationTopConstraint : NSLayoutConstraint!
    
    var navigationBar : AnytimeNavigationBar!
    var dateViewHeightConstraint : NSLayoutConstraint!
    var navigationBarTopConstraint : NSLayoutConstraint!

    var closedTrackerDigitsScale : CGFloat = 0.35
    var closedTrackerHeight : CGFloat = 88
    var openedTrackerHeight : CGFloat = 200
    
    var datePickerHeight : CGFloat = 100
    
    var trackerCategoryLabelTopConstraint : NSLayoutConstraint!
    
    var timeEntryKeyboard : TimeEntryKeyboard!
    var timeEntryKeyboardBottomPosition : NSLayoutConstraint!
    var timeEntryKeyboardHeightConstraint : NSLayoutConstraint!
    
    var timeRangeViewController : TimeRangeViewController!

    override func loadView() {
        self.view = DraggableView(frame: UIScreen.mainScreen().applicationFrame)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        backgroundView = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.Dark))
        self.view .addSubview(backgroundView)
        //backgroundView.backgroundColor = UIColor(red: 0.523, green: 0.7, blue: 0.889, alpha: 0.5)
        //backgroundView.backgroundColor = UIColor(red: 0.635, green: 0.79, blue: 0.999, alpha: 0.75)
//        backgroundView.backgroundColor = UIColor(red: 0.212, green: 0.251, blue: 0.306, alpha: 0.5)
       //self.view.backgroundColor = UIColor(red: 0.21, green: 0.229, blue: 0.255, alpha: 0.5)
        
        var backgroundImage = UIImageView(image: UIImage(named: "backdground_dark"))
        backgroundImage.contentMode = UIViewContentMode.ScaleToFill
        backgroundImage.alpha = 0.75
        backgroundImage.backgroundColor = UIColor(red: 0.216, green: 0.278, blue: 0.31, alpha: 1)
        //backgroundImage.backgroundColor = UIColor(white: 0, alpha: 0.2)
        //backgroundImage.backgroundColor = UIColor.blackColor()
        
        //self.view.backgroundColor = UIColor(white: 0, alpha: 0.75)
        
        backgroundView.addSubview(backgroundImage)
        
        layout(backgroundView, self.view) { view, superview in
            view.top == superview.top
            view.centerX == superview.centerX
            view.bottom == superview.bottom + 20
            view.width == superview.width
        }
        
       layout(backgroundImage, backgroundView) { view, superview in
           view.top == superview.top
           view.centerX == superview.centerX
           view.bottom == superview.bottom
           view.width == superview.width
       }
        
        addButton = UIButton(frame: CGRectMake(0, 0, 44, 44))
        addButton.setImage(UIImage(named: "button_add"), forState: UIControlState.Normal)
        addButton.tintColor = UIColor.whiteColor()

        self.view .addSubview(addButton)
        
        layout(addButton, self.view) { view, superview in
            view.centerX == superview.centerX
            view.top == superview.top + 15
            view.width == 60
            view.height == view.width
        }

        var panGesture = UIPanGestureRecognizer(target: self, action: "handlePan:")
        panGesture.delegate = self
        self.view.addGestureRecognizer(panGesture)
        
        
        navigationBar = AnytimeNavigationBar(frame: CGRectZero)
        navigationBar.delegate = self
        self.view .addSubview(navigationBar)
        layout(navigationBar, self.view) { navigationBar, superview in
            self.navigationBarTopConstraint = navigationBar.top == superview.top
            navigationBar.centerX == superview.centerX
            self.dateViewHeightConstraint = navigationBar.height == 0
            navigationBar.width == superview.width
        }
        
        var dateViewLine = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.Light))
        dateViewLine.alpha = 0
        self.view .addSubview(dateViewLine)
        
        layout(dateViewLine, navigationBar) { dateViewLine, dateView in
            dateViewLine.width == dateViewLine.superview!.width
            dateViewLine.top == dateView.bottom
            dateViewLine.centerX == dateView.centerX
            dateViewLine.height == 1
        }
        
        timeDurationViewController = TimeDurationViewController(nibName: nil, bundle: nil)
        timeDurationViewController.delegate = self
        self.view .addSubview(timeDurationViewController.view)
        
        layout(timeDurationViewController.view, dateViewLine) { view, dateViewLine in
            view.centerX == view.superview!.centerX
            view.width == view.superview!.width
            self.timeDurationTopConstraint = view.top == dateViewLine.bottom
        }

        timeDurationViewController.setScale(0)
        
        var vibrantLine = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.Light))
        vibrantLine.alpha = 0.25

        self.view .addSubview(vibrantLine)
        
        layout(vibrantLine, timeDurationViewController.view) { vibrantLine, digitsView in
            vibrantLine.width == digitsView.width
            vibrantLine.top == digitsView.bottom
            vibrantLine.centerX == digitsView.centerX
            vibrantLine.height == 1
        }
        
        
        addButton.hidden = true
        // Do any additional setup after loading the view.
        
        navigationBar.alpha = 0
        
        timeRangeViewController = TimeRangeViewController(nibName: nil, bundle: nil)
        timeRangeViewController.delegate = self
        self.view .addSubview(timeRangeViewController.view)
        
        layout(timeRangeViewController.view, vibrantLine) { view, upperView in
           view.width == view.superview!.width
           view.centerX == view.superview!.centerX
           view.top == upperView.bottom
           self.timeRangeViewController.heightConstraint = view.height == Float(self.timeRangeViewController.preferredHeight())
        }
       

        var trackerCategoryLabel = TrackerCategoryView(frame: CGRectZero)
        self.view.addSubview(trackerCategoryLabel)
        
        layout(trackerCategoryLabel, timeRangeViewController.view) { view, toDigits in
            view.centerX == view.superview!.centerX
            self.trackerCategoryLabelTopConstraint = view.top == toDigits.bottom
            view.width == view.superview!.width
            view.height == 100
        }
        
        trackerCategoryLabel.mainCategoryLabel.text = "Anytime App"
    
        trackerCategoryLabel.subCategoryLabel.text = "User Interface Design"
        
        var startButton = UIButton(frame: CGRectZero)
        startButton.setImage(UIImage(named: "icon_play"), forState: UIControlState.Normal)
        view.addSubview(startButton)
        startButton.tintColor = UIColor(red: 0.335, green: 0.765, blue: 0.444, alpha: 1)
        layout(startButton, trackerCategoryLabel) { view, trackerCategoryLabel in
            view.centerX == view.superview!.centerX
            view.top == trackerCategoryLabel.bottom + 30
            view.height == 80
            view.width == view.height
        }

        self.view.bringSubviewToFront(navigationBar)
        self.view.bringSubviewToFront(timeDurationViewController.view)


    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        navigationBar.rightBarButtonItem = UIBarButtonItem(frame: CGRectMake(0, 0, 32, 32), buttonType: FlatButtonType.buttonDownBasicType, buttonStyle: FlatButtonStyle.buttonRoundedStyle, animateToInitialState: true)
        
        self.navigationBar.rightBarButtonItem.setTintColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        self.navigationBar.rightBarButtonItem.setTintColor(UIColor.greenColor(), forState: UIControlState.Highlighted)
        
        self.navigationBar.rightBarButtonItem.addTarget(self, action: "closeFromTo", forControlEvents: UIControlEvents.TouchUpInside)

        
    }
    
    func attachVibrantDividerToView(view: UIView!) -> (UIView!, NSLayoutConstraint!) {
        
        var vibrantLine = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.Light))
        vibrantLine.alpha = 0.25
        
        self.view .addSubview(vibrantLine)
        
        var layoutConstraint : NSLayoutConstraint!
        
        layout(vibrantLine, view) { vibrantLine, digitsView in
            vibrantLine.width == digitsView.width
            layoutConstraint = vibrantLine.top == digitsView.bottom
            vibrantLine.centerX == digitsView.centerX
            vibrantLine.height == 1
        }
        
        return (vibrantLine, layoutConstraint)
    }

    func attachVerticalDividerToView(view: UIView!) -> (UIView!, NSLayoutConstraint!) {
        
        var vibrantLine = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.Light))
        vibrantLine.alpha = 0.25
        
        self.view .addSubview(vibrantLine)
        
        var layoutConstraint : NSLayoutConstraint!
        
        layout(vibrantLine, view) { vibrantLine, digitsView in
            vibrantLine.width == 1
            layoutConstraint = vibrantLine.top == digitsView.top
            vibrantLine.left == digitsView.right
            vibrantLine.height == digitsView.height
        }
        
        return (vibrantLine, layoutConstraint)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
}

/*!
*  MARK: - Pan Gesture Recognizer
*/

extension TrackerViewController {
    
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWithGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
    
    func gestureRecognizer(gestureRecognizer: UIGestureRecognizer, shouldBeRequiredToFailByGestureRecognizer otherGestureRecognizer: UIGestureRecognizer) -> Bool {
    
        if otherGestureRecognizer.isKindOfClass(UIPanGestureRecognizer.classForCoder()) {
            return false;
        } else {
            return true;
        }
    }
    
    func percentageForCurrentPosition() -> CGFloat {
        var currentPoint = view.layer.presentationLayer().position
        var superviewCenter = view.superview!.center
        var distance2 = currentPoint.y - superviewCenter.y

        var distance = 1 - (fabs(currentPoint.y - superviewCenter.y) / (view.bounds.size.height - 88))
        
        return distance
    }
    
    func stopAllAnimations() {
        var percentage = percentageForCurrentPosition()
        
        
        var point = view.layer.presentationLayer().position
        
        self.view.center = point
        view.layer.removeAllAnimations()
        
        
        for subview in self.view.subviews {
            subview.layer.removeAllAnimations()
            for view in subview.subviews {
                view.layer.removeAllAnimations()
            }
        }
        
        timeDurationViewController.view.layer.removeAllAnimations()
        
        setTransitionProgress(percentage)
        
        self.view.layoutIfNeeded()

    }
    
    func handlePan(gesture: UIPanGestureRecognizer) {
        
        stopAllAnimations()
        
        var locationInView = gesture.locationInView(gesture.view!.superview)
        
        var superViewHeight = gesture.view!.superview!.bounds.size.height
        
        var locationInTracker = gesture.locationInView(gesture.view!)
        
        var distanceLeft = self.view.frame.origin.y

        var velocityInView = gesture.velocityInView(gesture.view)

//        if velocityInView.y < 0 {
//            distanceLeft = gesture.view!.superview!.bounds.size.height - locationInView.y
//        }
//        
        
        if (velocityInView.y > 0) {
            distanceLeft = gesture.view!.superview!.bounds.size.height - self.view.frame.origin.y
        }
        
        var duration : NSTimeInterval = Double(distanceLeft / gesture.view!.superview!.bounds.size.height)

        
        if gesture.state == UIGestureRecognizerState.Began {
            
            initialPanPoint = locationInTracker
            
            if velocityInView.y > 0 {

            } else {

            }
        
        }
        
        if gesture.state == UIGestureRecognizerState.Ended {
            
            println(distanceLeft)

            var normalizedInitialVelocity = fabs(velocityInView.y / distanceLeft)
            
            if normalizedInitialVelocity > 0 {
               // normalizedInitialVelocity = min(normalizedInitialVelocity, 12)
            }
            
            if (normalizedInitialVelocity < 0) {
             //   normalizedInitialVelocity = max(normalizedInitialVelocity, -12)
            }
            
            normalizedInitialVelocity = fabs(normalizedInitialVelocity / 12) * 2
            
            if velocityInView.y > 0 {
            } else {

            }
            
            
            UIView.animateWithDuration(duration, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 2 + normalizedInitialVelocity, options: UIViewAnimationOptions.AllowUserInteraction , animations: { () -> Void in
                
                if velocityInView.y > 0 {
                    self.setTransitionProgress(0)
                    
                    self.addButton.alpha = 1
                    //  self.digitsView.alpha = 0
                    
                } else {
                    self.setTransitionProgress(1)
                    
                    self.addButton.alpha = 0
                    
                }
                
                self.view.layoutIfNeeded()
                
                }) { (finished: Bool) -> Void in
                    
            }
            
        }
        
        if gesture.state == UIGestureRecognizerState.Changed {
            
            var ditanceFromBottom = -(superViewHeight - locationInView.y ) - initialPanPoint.y
            var percentage = 1 - (self.view.bounds.size.height + ditanceFromBottom) / (self.view.bounds.size.height - 88)
            bottomPositionConstraint.constant = ditanceFromBottom
            
//            println("Changed \(percentage)")
            setTransitionProgress(percentage)

        }
        
        
    }

    func setTransitionProgress(progress : CGFloat) {
      var superViewHeight = self.view.superview!.bounds.size.height
      

        self.bottomPositionConstraint.constant = -closedTrackerHeight + (-superViewHeight + closedTrackerHeight) * progress
        self.dateViewHeightConstraint.constant = self.datePickerHeight * progress
        
        timeDurationViewController.setScale(progress)
        
        navigationBar.alpha = (progress - 0.75) + (progress * 0.75)
    }
    
    
    func timeRangeViewController(timeRangeViewController: TimeRangeViewController, didSelectDigitsView digitsView: DigitsView!) {
                
        UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 2.5, options: UIViewAnimationOptions.AllowUserInteraction, animations: { () -> Void in
            
            self.view.layoutIfNeeded()
            }) { (finsihed: Bool) -> Void in
                self.view.layoutIfNeeded()
        }
    
    }
    
    func timeRangeViewController(timeRangeViewController: TimeRangeViewController, willTransitionToState toState: TimeRangeState, fromState: TimeRangeState) -> (TransitionAnimation?) {

        if ((fromState == .NotEditing || toState == .NotEditing) && fromState != toState ) {
            
            if (toState != .NotEditing) {
                self.transitionToMode(.EditingTimeRange)
            }

            
            return TransitionAnimation(duration: 0.5, animations: { () -> () in
                
                if (toState != .NotEditing) {
                    self.timeDurationViewController.slideDigitsOut()
                } else {
                    self.setTransitionProgress(1)
                }
                
                self.view.layoutIfNeeded()
                
            })
        }
        return nil
    }
    
    func timeRangeViewController(timeRangeViewController: TimeRangeViewController, didTransitionToState toState: TimeRangeState, fromState: TimeRangeState) {
        
        
    }
    
    func closeFromTo() {

        self.transitionToMode(.NotEditing)

    }
    
    func transitionToMode(mode: TrackerMode) {
        
        self.mode = mode
        
        if mode == .EditingTimeRange {
            self.navigationBar.titleView.alpha = 0
            self.timeDurationViewController.transitionToMode(.NotEditing)
            UIView.performWithoutAnimation({ () -> Void in
                self.navigationBar.rightBarButtonItem.animateToType(FlatButtonType.buttonCloseType)
            })
        }
        
        if mode == .EditingDuration {
            UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 2.5, options: UIViewAnimationOptions.AllowUserInteraction, animations: { () -> Void in
                
                self.navigationBar.titleView.alpha = 1
                self.navigationBar.titleView.transform = CGAffineTransformMakeScale(0.75, 0.75)
                }) { (finsihed: Bool) -> Void in
            }

            self.timeRangeViewController.transistionToState(.NotEditing, animated: true)
        }
        
        if mode == .NotEditing {
            
            UIView.animateWithDuration(0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 2.5, options: UIViewAnimationOptions.AllowUserInteraction, animations: { () -> Void in
                
                self.navigationBar.titleView.alpha = 1
                self.navigationBar.titleView.transform = CGAffineTransformMakeScale(1, 1)
                }) { (finsihed: Bool) -> Void in
            }
            
            self.navigationBar.rightBarButtonItem?.animateToType(FlatButtonType.buttonDownBasicType)
            timeRangeViewController.transistionToState(TimeRangeState.NotEditing, animated: true)
            timeDurationViewController.transitionToMode(.NotEditing)
        }
        
    }
}

extension TrackerViewController {
    
    func navigationBar(navigationBar: AnytimeNavigationBar, didSelectTitleView titleView: DateView) {
        
        if (mode == .EditingTimeRange) {
            self.timeDurationViewController.transitionToMode(.Editing)
        }
    
    }
    
    func timeDurationViewController(timeDurationViewController: TimeDurationViewController, didSwitchMode mode: TimeDurationEditingMode) {

        if mode == .Editing {
            
            UIView.performWithoutAnimation({ () -> Void in
                self.navigationBar.rightBarButtonItem.animateToType(FlatButtonType.buttonCloseType)
            })
            
            
            self.transitionToMode(.EditingDuration)
            self.timeDurationTopConstraint.constant = 0

        } else {
            

        }
    }
}


