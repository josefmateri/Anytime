//
//  TimeRangeViewController.swift
//  Anytime
//
//  Created by Josef Materi on 25.10.14.
//  Copyright (c) 2014 Crush & Lovely. All rights reserved.
//

import UIKit
import Cartography

protocol TimeRangeViewContollerDelegate {
    func timeRangeViewController(timeRangeViewController: TimeRangeViewController, willTransitionToState toState:TimeRangeState, fromState: TimeRangeState) -> (TransitionAnimation?)
    func timeRangeViewController(timeRangeViewController: TimeRangeViewController, didTransitionToState toState:TimeRangeState, fromState: TimeRangeState)
}

enum TimeRangeState {
    case EditingFrom
    case EditingTo
    case NotEditing
}

struct TimeRangeConfiguration {
    
    var fromMinutes : Int
    var toMinutes : Int
    
    init(fromMinutes: Int, toMinutes: Int) {
        self.fromMinutes = fromMinutes
        self.toMinutes = toMinutes
    }
}
    
class TimeRangeViewController: UIViewController, UIGestureRecognizerDelegate, TimeEntryKeyboardDelegate {

    var toDigits: DigitsView = DigitsView(frame: CGRectZero)
    var fromDigits: DigitsView = DigitsView(frame: CGRectZero)
    var keyboard : TimeEntryKeyboard!

    var delegate : TimeRangeViewContollerDelegate?
    
    var state : TimeRangeState = .NotEditing {
        didSet {
            self.toDigits.backgroundColor = UIColor.clearColor()
            self.fromDigits.backgroundColor = UIColor.clearColor()
            
            if self.state == .EditingFrom {
                self.fromDigits.backgroundColor = UIColor(white: 0, alpha: 0.1)
            }
            
            if self.state == .EditingTo {
                self.toDigits.backgroundColor = UIColor(white: 0, alpha: 0.1)
            }
        }
    }

    var timeRangeConfiguration : TimeRangeConfiguration?
    
    var currentDigits: DigitsView? {
        get {
            switch self.state {
            case TimeRangeState.EditingFrom:
                return fromDigits
            case TimeRangeState.EditingTo:
                return toDigits
            default:
                return nil
            }
            
        }
    }
    
    var digitsHeight : Float = 120
    
    var heightConstraint : NSLayoutConstraint?
    var timeEntryKeyboardHeightConstraint : NSLayoutConstraint!

    override func viewDidLoad() {
        super.viewDidLoad()
        
        fromDigits.font = UIFont(name: "HelveticaNeue", size: 42)
        self.view .addSubview(fromDigits)
        layout(fromDigits, self.view) { fromDigits, superview in
            fromDigits.top == superview.top
            fromDigits.width == superview.width / 2
            fromDigits.left == superview.left
            fromDigits.height == self.digitsHeight
        }
        
        fromDigits.setTitle("FROM")
        fromDigits.titleLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 16)
        fromDigits.setTime(minutes: 111)

        toDigits.font = UIFont(name: "HelveticaNeue", size: 42)
        self.view .addSubview(toDigits)
        layout(toDigits, self.view) { toDigits, superview in
            toDigits.top == superview.top
            toDigits.width == superview.width / 2
            toDigits.right == superview.right
            toDigits.height == self.digitsHeight
        }
        
        toDigits.setTitle("TO")
        toDigits.titleLabel.font = UIFont(name: "HelveticaNeue-Thin", size: 16)
        toDigits.setTime(minutes: 111)
        
        var toDigitsTapGesture = UILongPressGestureRecognizer(target: self, action: "didTapToDigits:")
        toDigitsTapGesture.minimumPressDuration = 0
        toDigitsTapGesture.delegate = self
        
        var fromDigitsTapGesture = UILongPressGestureRecognizer(target: self, action: "didTapFromDigits:")
        fromDigitsTapGesture.minimumPressDuration = 0
        fromDigitsTapGesture.delegate = self
        
        toDigits.addGestureRecognizer(toDigitsTapGesture)
        fromDigits.addGestureRecognizer(fromDigitsTapGesture)

        
        attachVerticalDividerToView(fromDigits)
        attachVibrantDividerToView(fromDigits)
        attachVibrantDividerToView(toDigits)
        
        keyboard = TimeEntryKeyboard(frame: CGRectZero)
        keyboard.delegate = self
        self.view .addSubview(keyboard!)
        
        layout(keyboard!, toDigits) { keyboard, upperView in
            keyboard.top == upperView.bottom
            keyboard.centerX == keyboard.superview!.centerX
            keyboard.width == keyboard.superview!.width
            self.timeEntryKeyboardHeightConstraint = keyboard.height == Float(self.keyboard!.heightForState(KeyboardState.Closed))
        }

    }

    func preferredHeight() -> CGFloat {
    
        if state == TimeRangeState.NotEditing {
            return toDigits.bounds.size.height
        } else {
            return toDigits.bounds.size.height + self.timeEntryKeyboardHeightConstraint.constant
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    
    func setKeyboardScale(scale: CGFloat) {
        self.keyboard?.collectionView.transform = CGAffineTransformMakeScale(scale, scale)
        self.keyboard?.keyboardModeView.collectionView.transform = CGAffineTransformMakeScale(scale, scale)
    }
    
    func unselectAllDigitsViews() {
        toDigits.unselectAllDigits()
        fromDigits.unselectAllDigits()
    }
    
    func transistionToState(newState: TimeRangeState, animated: Bool) {
        
        self.unselectAllDigitsViews()
        
        var oldState = state
        self.state =  newState;
        
        let transitionAnimation = delegate?.timeRangeViewController(self, willTransitionToState: state, fromState: oldState)
        
        var duration = 0.5
        
        if let unboxedDuration = transitionAnimation?.duration {
            duration = unboxedDuration
        }
        
        var keyboardState = KeyboardState.Open
        if newState == .NotEditing {
            keyboardState = .Closed
        }
        
        
        prepareDigitForMode(keyboard.mode)
        
        UIView.animateWithDuration(duration, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: UIViewAnimationOptions.AllowUserInteraction , animations: { () -> Void in
            
          self.timeEntryKeyboardHeightConstraint.constant = self.keyboard!.heightForState(keyboardState)
          
          self.heightConstraint?.constant = self.preferredHeight()
            
            switch self.state {
          case TimeRangeState.EditingFrom:
              self.setKeyboardScale(1)
          case TimeRangeState.EditingTo:
              self.setKeyboardScale(1)
          default:
              self.setKeyboardScale(0.75)
          }

            self.view.layoutIfNeeded()
            
            if let unboxedAnimationBlock = transitionAnimation?.animations {
                unboxedAnimationBlock()
            }
            
            }) { (finished: Bool) -> Void in
                
        }
        
    }
    
    
    func didTapToDigits(gesture: UITapGestureRecognizer) {
        
//        self.toDigits.transform = CGAffineTransformMakeScale(0.75, 0.75)
//
//        UIView.animateWithDuration(0.25, delay: 0, usingSpringWithDamping: 0.3, initialSpringVelocity: 0, options: UIViewAnimationOptions.AllowUserInteraction, animations: { () -> Void in
//            
//            self.toDigits.transform = CGAffineTransformMakeScale(1, 1)
//        
//            }) { (finished: Bool) -> Void in
//            
//        }
//        
        if gesture.state == .Ended {
            self.transistionToState(TimeRangeState.EditingTo, animated: true)
        }
    }
    
    func didTapFromDigits(gesture: UITapGestureRecognizer) {
        if gesture.state == .Ended {
            self.transistionToState(TimeRangeState.EditingFrom, animated: true)
        }
    }
    
    func attachVerticalDividerToView(view: UIView!) -> (UIView!, NSLayoutConstraint!) {
        
        var vibrantLine = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.Light))
        vibrantLine.alpha = 0.25
        
        self.view .addSubview(vibrantLine)
        
        var layoutConstraint : NSLayoutConstraint!
        
        layout(vibrantLine, view) { vibrantLine, digitsView in
            vibrantLine.width == 1
            layoutConstraint = vibrantLine.top == digitsView.top
            vibrantLine.left == digitsView.right
            vibrantLine.height == digitsView.height
        }
        
        return (vibrantLine, layoutConstraint)
    }

    func attachVibrantDividerToView(view: UIView!) -> (UIView!, NSLayoutConstraint!) {
        
        var vibrantLine = UIVisualEffectView(effect: UIBlurEffect(style: UIBlurEffectStyle.Light))
        vibrantLine.alpha = 0.25
        
        self.view .addSubview(vibrantLine)
        
        var layoutConstraint : NSLayoutConstraint!
        
        layout(vibrantLine, view) { vibrantLine, digitsView in
            vibrantLine.width == digitsView.width
            layoutConstraint = vibrantLine.top == digitsView.bottom
            vibrantLine.centerX == digitsView.centerX
            vibrantLine.height == 1
        }
        
        return (vibrantLine, layoutConstraint)
    }
    
    func setNextState() {
        if state == .EditingTo {
            transistionToState(TimeRangeState.NotEditing, animated: true)
        }
        if state == .EditingFrom {
            self.currentDigits?.unselectAllDigits()
            self.state = .EditingTo
            self.currentDigits?.selectedIndex = 0
        }
    }
    
    // TODO: Improve digit selection api to be more consitent unselectAllDigits <=> selectedIndex
    func timeEntryKeyboard(keyboard: TimeEntryKeyboard, didSelectDigit: Int) {
        if (self.currentDigits?.setDigitAtSelectedIndex(didSelectDigit) == true) {
            if (self.currentDigits?.jumpToNextDigit() == false) {
                setNextState()
            }
        }
        updateTimeRangeConfiguration()
    }
    
    func timeEntryKeyboard(keyboard: TimeEntryKeyboard, didSwitch mode: KeyboardMode) {        
        prepareDigitForMode(mode)
    }
    
    func prepareDigitForMode(mode: KeyboardMode) {
        if (mode == .Time || mode == .History) {
            self.currentDigits?.selectAllDigits()
        }
        if (mode == .Digits) {
            self.currentDigits?.selectedIndex = 0
        }
    }
    
    func updateTimeRangeConfiguration() {
        timeRangeConfiguration?.toMinutes = toDigits.minutes!
        timeRangeConfiguration?.fromMinutes = fromDigits.minutes!
    }
    
}