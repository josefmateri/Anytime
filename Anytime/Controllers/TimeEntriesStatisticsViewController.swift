//
//  TimeEntriesStatisticsViewController.swift
//  Anytime
//
//  Created by Josef Materi on 01.11.14.
//  Copyright (c) 2014 Crush & Lovely. All rights reserved.
//

import UIKit
import Cartography

class TimeEntriesStatisticsViewController: UIViewController {

    var yOffsetConstraint : NSLayoutConstraint!
    
    var progressView : MDRadialProgressView!
    var progressTheme : MDRadialProgressTheme!
    
    var contentInsets : UIEdgeInsets = UIEdgeInsetsMake(40,0,0,0) {
        didSet {
             self.yOffsetConstraint.constant = self.contentInsets.top
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        progressTheme = MDRadialProgressTheme()
        progressTheme.completedColor = UIColor.whiteColor().colorWithAlphaComponent(1)
        progressTheme.incompletedColor = UIColor.whiteColor().colorWithAlphaComponent(0.1)
        progressTheme.labelShadowColor = UIColor.clearColor().colorWithAlphaComponent(0)
        progressTheme.thickness = 20
        progressTheme.sliceDividerHidden = true
        
        progressView = MDRadialProgressView(frame: CGRectMake(0, 0, 100, 100), andTheme: progressTheme)
        progressView.progressTotal = 300
        progressView.progressCounter = 200
        progressView.label.hidden = true
        
        progressView.theme = progressTheme
        
        var testLabel = UILabel(frame: CGRectZero)
        self.view.addSubview(progressView)
        
        var height = self.view.bounds.size.height
        
        layout(progressView, self.view) { view, superview in
            view.centerX == superview.centerX
            self.yOffsetConstraint = view.centerY == superview.centerY + Float(self.contentInsets.top)
           // view.centerY == superview.centerY
            view.width == 100
            view.height == view.width
        }
        
        var minutesLabel = UILabel(frame: CGRectZero)
        progressView.addSubview(minutesLabel)
        
        minutesLabel.font = UIFont(name: "HelveticaNeue", size: 24)
        
        layout(minutesLabel, progressView) { view, superview in
            view.centerX == superview.centerX
            view.centerY == superview.centerY
        }
        
        minutesLabel.textColor = UIColor.whiteColor()
        minutesLabel.text = "09:30"

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    func setScale(scale: CGFloat) {
        progressView.transform = CGAffineTransformMakeScale(scale, scale)
    }
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
