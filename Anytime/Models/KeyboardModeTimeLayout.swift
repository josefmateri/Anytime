//
//  KeyboardModeTimeLayout.swift
//  Anytime
//
//  Created by Josef Materi on 26.10.14.
//  Copyright (c) 2014 Crush & Lovely. All rights reserved.
//

import UIKit

class KeyboardModeTimeLayout: NSObject, KeyboardModeLayoutDatasourceProtocol {
   
    func keyDataForIndexPath(indexPath: NSIndexPath) -> KeyData {
        
        var title = "00:00"
        var value = 0
        
        return KeyData(title: title, value: value, enabled: true)
    }
}
