//
//  KeyboardModeLayoutDatasource.swift
//  Anytime
//
//  Created by Josef Materi on 26.10.14.
//  Copyright (c) 2014 Crush & Lovely. All rights reserved.
//

import UIKit



class KeyboardModeDigitLayout: NSObject, KeyboardModeLayoutDatasourceProtocol {
    
    override init() {
        super.init()
    }
    
    func keyDataForIndexPath(indexPath: NSIndexPath) -> KeyData {
        
        var title = String(indexPath.row + 1)
        var value = indexPath.row + 1
        var enabled = true
        
        if (indexPath.row == 10) {
            title = "0"
            value = 0
        }
        
        if (indexPath.row == 9 || indexPath.row == 11) {
            enabled = false
        }
        
        return KeyData(title: title, value: value, enabled: enabled)
    }
    
}