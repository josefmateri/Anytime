//
//  TransitionAnimation.swift
//  Anytime
//
//  Created by Josef Materi on 25.10.14.
//  Copyright (c) 2014 Crush & Lovely. All rights reserved.
//

import UIKit

class TransitionAnimation: NSObject {
    
    var animations : (() -> ())?
    var duration : NSTimeInterval!
    
    override init() {
        super.init()
        duration = 0.5
    }
    
    convenience init(duration: NSTimeInterval = 0.5, animations: () -> ()) {
        self.init()
        self.duration = duration
        self.animations = animations
    }
    
}
