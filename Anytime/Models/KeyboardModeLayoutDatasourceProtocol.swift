//
//  KeyboardModeLayoutDatasourceProtocol.swift
//  Anytime
//
//  Created by Josef Materi on 26.10.14.
//  Copyright (c) 2014 Crush & Lovely. All rights reserved.
//

import UIKit

struct KeyData {
    var title : String!
    var value : Int!
    var enabled: Bool = true

    init(title: String?, value: Int?, enabled : Bool = true) {
        self.title = title
        self.value = value
        self.enabled = enabled
    }
}

protocol KeyboardModeLayoutDatasourceProtocol {
    func keyDataForIndexPath(indexPath: NSIndexPath) -> KeyData
}